﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ThreeJSSite.Startup))]
namespace ThreeJSSite
{
    public partial class Startup
    {
	//11
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}
