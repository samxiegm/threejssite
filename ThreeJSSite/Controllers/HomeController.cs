﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ThreeJSSite.Controllers
{
    public class HomeController : Controller
    {
        static ItemStateHub hub;

        public ActionResult Index()
        {
            hub = new ItemStateHub();
            Task.Factory.StartNew(async () => 
            {
                await Task.Delay(5000);
                hub.Send(new List<BagPosition> { new BagPosition() {
                    BagId = 1,
                    X = 0,
                    Y = 0,
                    Z = 0
                } });
            });

            return View();
        }

        public void About()
        {
            hub.Send(new List<BagPosition> { new BagPosition() {
                BagId = 1,
                X = 0,
                Y = 0,
                Z = 0
            } });
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}