﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Hubs;

namespace ThreeJSSite
{
    public class BagPosition
    {
        public int BagId { get; set; }

        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
    }

    [HubName("itemStateHub")]
    public class ItemStateHub : Hub
    {
        public override Task OnConnected()
        {
            string connectionId = Context.ConnectionId;
            return base.OnConnected();
        }

        public void Send(List<BagPosition> bagPositions)
        {
            // Call the broadcastMessage method to update clients.
            var context = GlobalHost.ConnectionManager.GetHubContext<ItemStateHub>();
            context.Clients.All.updateBagPosition(bagPositions);
        }
    }
}